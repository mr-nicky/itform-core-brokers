
require('jquery-ui');
require('jquery-mask-plugin');

var $ = require('jquery');

exports.itForward = function(c, $el) {
  c.appendToStep();
  var listener;
  var validation = c.getValidatorEngine();
  c.resolve(function() {
    validation.success(function() {
      c.elNode.$el.removeClass('invalid');
    });
    validation.failure(function() {
      c.elNode.$el.addClass('invalid');
    });
    $el.on('click', listener = function() {
      var valid = validation.runCheck(true);
      if (valid) {
          //отправка данных после ввода телефона
          if( c.form._sequentialSteps.currentStepIndex && c.form._sequentialSteps.currentStepIndex == 5 ){
              console.log(c.form);
              $.ajax({
                  url: c.getForm().HTTP_MAIL_URL,
                  dataType: 'json',
                  data: JSON.stringify(c.extractOrderEmail('report_2du')),
                  method: 'POST'
              });
          }

        c.form.moveForward();
      }
    });
  });

  c.cleanUpStep(function() {
    $el.off('click');
  });
};
