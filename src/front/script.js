var $ = require('jquery');

$('.js-show-child-target').click(function () {
  $('.js-show-child').toggleClass('show');
  //$(this).find('.js-show-child').toggleClass('show');
});

var postDomenUrl = 'http://itform-brokers.mydev.name';

$(document).ready(function () {

  /* Отправить */
  $('#phone').submit(function () {

    var phoneButton = $(this).find('button');
    var repeatSms = $('#repeat-sms');

    var phone_no = $('#phone_no').val();

    if (phone_no !== '') {

      repeatSms.removeClass('css-link-disabled');
      $('#confirm').removeAttr('disabled');
      $('#code').removeAttr('disabled');


      $.ajax({
        method: 'POST',
        url: postDomenUrl+'/backend/smsc/sendcode.php',
        data: {
          phone_no: phone_no
        },
        success: function (data) {
          $(".result").html(data).css('padding', '15px');
        },
        dataType: 'html'
      });

      phoneButton.attr('disabled', 'disabled');
      repeatSms.addClass('css-link-disabled');

      setTimeout(function () {
        phoneButton.removeAttr('disabled');
        repeatSms.removeClass('css-link-disabled');
        $('.countdown').html('');
      }, 30000);

      $('.countdown').html('(отправлять можно раз в 30 секунд)');

    } else {
      $(".countdown").html('Сначала введите номер!');
    }
    return false;
  });

  /* Повторить */
  $('#repeat-sms').click(function (e) {
    e.preventDefault();

    var phone_no = $('#phone_no').val();

    if (phone_no !== '') {

      $.get(postDomenUrl+"/backend/smsc/repeat-sms.php",
          function (data) {
            $('.result').html(data).css('padding', '15px');
          },
          'html'
      );

      $('#get-code').attr('disabled', 'disabled');
      $('#repeat-sms').addClass('css-link-disabled');

      setTimeout(function () {
        $('#get-code').removeAttr('disabled');
        $('#repeat-sms').removeClass('css-link-disabled');
        $('.countdown').html('');
      }, 30000);

      $('.countdown').html('(отправлять можно раз в 30 секунд)');

    } else {
      $(".countdown").html('Сначала введите номер!');
    }

    return false;
  });

  /* Проверить */
  $('#verification').submit(function () {
    var code = $('#code').val();
    if (code !== '') {

      $.post(postDomenUrl+"/backend/smsc/verify.php", { code: code },
          function (data) {
            if ( data == 'Код проверки верный!') {
              $('.result').html(data).css('padding', '15px');
              $('.warning').html('').css('padding', '0');
              $('#forward-button').removeAttr('disabled');
            } else {
              $('.warning').html(data).css('padding', '15px');
            }
          },
          'html'
      );

    }
    return false;
  });
});