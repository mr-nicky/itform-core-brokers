function toggleDisplayOfElement(el, type) {
  if (el) {
    if (type) {
      el.style.display = type;

      return;
    }

    if (el.style.display === 'block') {
      el.style.display = 'none';
    } else {
      el.style.display = 'block';
    }
  }
}

module.exports = toggleDisplayOfElement;