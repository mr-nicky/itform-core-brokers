var $ = require('jquery');

exports.itDeductibleValue = function(c, $el) {
  $el.on('change', function() {
    c.set('calc.deductible_value', $el.val().replace(/\s+/g, ''));
  });
};